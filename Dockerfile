FROM python:3.9

COPY custom_explainer/agenetcaptumexplainer agenetcaptumexplainer
COPY pre_trained_model agenetcaptumexplainer/pre_trained_model
RUN pip install --no-cache-dir -e ./agenetcaptumexplainer

ENTRYPOINT ["python", "-m", "agenetcaptumexplainer"]