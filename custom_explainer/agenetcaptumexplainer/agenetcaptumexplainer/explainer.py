from typing import Dict

import logging
import kserve
import numpy as np
import io
import importlib
import sys
import os
import inspect
import base64

import torch
from torch import Tensor
import torch.nn.functional as F
from captum.attr import visualization as viz
from captum.attr import Occlusion

import nest_asyncio

nest_asyncio.apply()

PATH_TO_MODEL = 'pre_trained_model'


class CustomExplainer(kserve.Model):  # pylint:disable=c-extension-no-member
    def __init__(self, name: str):
        super().__init__(name)
        self.name = name
        self.model_class_name = 'Net'
        self.model_file = f'{PATH_TO_MODEL}/model.pt'
        self.model_class_module = f'{PATH_TO_MODEL}.model'
        self.ready = False
        self.model = None
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    def load(self) -> bool:
        # Load the python class into memory
        current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        parent_dir = os.path.dirname(current_dir)
        sys.path.insert(0, parent_dir)
        model_class = getattr(importlib.import_module(self.model_class_module), self.model_class_name)
        # Make sure the model weight is transform with the right device in this machine
        self.model = model_class().to(self.device)
        self.model.load_state_dict(torch.load(os.path.join(parent_dir, self.model_file), map_location=self.device))
        self.model.eval()
        self.ready = True

    def _predict(self, tensor: Tensor):
        resp = F.softmax(self.model(tensor), dim=1)
        prediction_score, pred_label_idx = torch.topk(resp, 1)
        pred_label_idx.squeeze_()
        return pred_label_idx

    def explain(self, payload: Dict, headers: Dict[str, str] = None): # headers is needed to overwrite the default kserve explain method
        instances = payload["instances"]
        try:
            inputs = np.array(instances[0]['data'])
            tensor = torch.tensor(inputs).unsqueeze_(0).float()
            logging.info("Calling explain on image of shape %s", (tensor.shape))
        except Exception as err:
            raise Exception(
                "Failed to initialize Tensor from inputs: %s, %s" % (err, instances))
        try:
            pred_label_idx = self._predict(tensor)
        except Exception as err:
            raise Exception("Failed to predict %s" % err)
        try:
            occlusion = Occlusion(self.model)
            attributions_occ = occlusion.attribute(tensor,
                                    target=pred_label_idx,
                                    strides=(3, 8, 8),
                                    sliding_window_shapes=(3, 15, 15),
                                    baselines=0)
            fig, ax = viz.visualize_image_attr_multiple(np.transpose(attributions_occ.squeeze().cpu().detach().numpy(), (1,2,0)),
                                    np.transpose(tensor.squeeze().cpu().detach().numpy(), (1,2,0)),
                                    ["original_image", "heat_map", "heat_map", "masked_image"],
                                    ["all", "positive", "negative", "positive"],
                                    show_colorbar=True,
                                    titles=["Original", "Positive Attribution", "Negative Attribution", "Masked"],
                                    fig_size=(18, 6),
                                    use_pyplot=False
                                     )
            memdata = io.BytesIO()
            fig.savefig(memdata, format='png', bbox_inches="tight")
            memdata.seek(0)
            encoded_string = base64.b64encode(memdata.read())
            explanationFig = encoded_string.decode("utf-8")
            return {
                "predictions": [
                    pred_label_idx
                ],
                "explanations": [
                    {
                        "b64": explanationFig
                    }
                ]
            }

        except Exception as err:
            raise Exception("Failed to explain %s" % err)
