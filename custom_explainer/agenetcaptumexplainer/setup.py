from setuptools import setup, find_packages

setup(
    name='captumcustomexplainer',
    version='0.2.0',
    packages=find_packages(include=['agenetcaptumexplainer', 'pre_trained_model']),
    author_email='tkleinloog@deeploy.ml',
    license='Apache 2.0',
    url='https://deeploy.ml',
    description='Model Explaination Server. \
                 Not intended for use outside KServe Frameworks Images',
    python_requires='>=3.6',
    install_requires=[
        "kserve==0.11.0",
        "pandas>=0.24.2",
        "nest_asyncio>=1.4.0",
        "argparse>=1.4.0",
        "requests>=2.22.0",
        "grpcio>=1.22.0",
        "numpy>=1.19.0",
        "torch==1.13",
        "torchvision==0.14",
        "captum==0.4.1",
        "matplotlib<3.7"
    ],
    package_data={'pre_trained_model': ['model.py', 'model.pt']},
)